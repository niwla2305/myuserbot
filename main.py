import datetime
import json
import logging
import os
import random
import re
import time
import asyncio

from telethon import TelegramClient, events
from dotenv import load_dotenv

load_dotenv()

api_id = os.getenv("api_id")
api_hash = os.getenv("api_hash")

client = TelegramClient('session_name', api_id, api_hash)
fff_correction_trusted = os.getenv("fff_correction_trusted").split(",")
fff_correction_trusted = [int(i) for i in fff_correction_trusted]
chatbot_trusted = os.getenv("chatbot_trusted").split(",")
chatbot_trusted = [int(i) for i in chatbot_trusted]
trusted_users = os.environ.get("trusted_users").split(",")
trusted_users = [int(i) for i in trusted_users]
hour_to_sleep = os.getenv("hour_to_sleep")

logging.basicConfig(format='[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s',
                    level=logging.WARNING)


@client.on(events.NewMessage(pattern=r'!nothing'))
async def handler32(event):
    await event.edit("ᅠᅠᅠᅠᅠᅠᅠᅠᅠᅠᅠᅠᅠ")


@client.on(events.NewMessage(pattern=r'!spam'))
async def handler4532(event):
    if event.from_id in trusted_users:
        arguments = event.raw_text.split(" ")
        repeat = arguments[1]
        message = " ".join(arguments[2::])
        await event.delete()
        async with client.action(event.chat, 'typing'):
            for i in range(0, int(repeat)):
                await event.respond(message)
                await asyncio.sleep(5)


@client.on(events.NewMessage(pattern=r'!split', outgoing=True, incoming=False))
async def split_fuck(event):
    async with client.action(event.chat, 'typing'):
        if event.from_id in trusted_users:
            words = event.raw_text.split(" ")[1::]
            if len(words) == 1:
                words = " ".join(words)
            await event.delete()
            for word in words:
                if word.strip() != "":
                    await asyncio.sleep(len(word) / 2)
                    await event.respond(word)


@client.on(events.NewMessage(pattern=r'!join', outgoing=True, incoming=False))
async def join(event):
    arguments = event.raw_text.split(" ")[1::]
    await event.edit(arguments[0].join(arguments[1::]))

char_emoji_mapping = {
    "a": "🄰",
    "b": "🄱",
    "c": "🄲",
    "d": "🄳",
    "e": "🄴",
    "f": "🄵",
    "g": "🄶",
    "h": "🄷",
    "i": "🄸",
    "j": "🄹",
    "k": "🄺",
    "l": "🄻",
    "m": "🄼",
    "n": "🄽",
    "o": "🄾",
    "p": "🄿",
    "q": "🅀",
    "r": "🅁",
    "s": "🅂",
    "t": "🅃",
    "u": "🅄",
    "v": "🅅",
    "w": "🅆",
    "x": "🅇",
    "y": "🅈",
    "z": "🅉",
    "ä": "🄰🄴",
    "ö": "🄾🄴",
    "ü": "🅄🄴"
}


@client.on(events.NewMessage(pattern=r'!boxedtext', outgoing=True, incoming=False))
async def boxed_text(event):
    text = ""
    input_text = " ".join(event.raw_text.split(" ")[1::])
    chars = list(input_text)
    for char in chars:
        try:
            text = text + char_emoji_mapping[char.lower()]
        except KeyError:
            text = text + char
    await event.edit(text)


@client.on(events.NewMessage(pattern=r'!cancer', outgoing=True, incoming=False))
async def cancer(event):
    before_big = False
    original = " ".join(event.raw_text.split(" ")[1::])
    result = ""
    for i in original:
        if before_big is True:
            new = i.upper()
        else:
            new = i.lower()
        result = result + new
        before_big = not before_big
    await event.edit(result)


@client.on(events.NewMessage())
async def handler2(event):
    if event.chat_id in fff_correction_trusted:
        if re.search(r"[Ff]f[Ff]", event.raw_text):
            await event.reply("*FFF")


@client.on(events.NewMessage())
async def why(event):
    if event.chat_id in chatbot_trusted:
        if datetime.datetime.now().hour < int(hour_to_sleep):
            await event.reply(
                random.choice([f"Es ist {datetime.datetime.now().strftime('%H:%M')}, ich schlafe.",
                               f"Es ist erst {datetime.datetime.now().strftime('%H:%M')}!"]))
        if "why" in event.raw_text:
            await event.reply("Because")


class SavedMessages:
    def __init__(self, file):
        self.messages = {}
        self.file = file
        if not os.path.isfile(self.file):
            with open(self.file, "w") as file2:
                file2.write("{}")

    def _read_config_file(self):
        with open(self.file, "r") as file:
            return json.loads(file.read())

    def _write_config_file(self, data):
        with open(self.file, "w") as file:
            file.write(json.dumps(data))

    def add_message(self, name, text):
        original = self._read_config_file()
        original[name] = text
        self._write_config_file(original)

    def get_message(self, name):
        return self._read_config_file()[name]


saved_messages = SavedMessages(file="saved.json")


@client.on(events.NewMessage(pattern=r'!save'))
async def save(event):
    to_save = await event.get_reply_message()
    name = event.raw_text[6:]
    saved_messages.add_message(name, to_save.text)


@client.on(events.NewMessage(pattern=r'!send'))
async def send(event):
    name = event.raw_text[6:]
    await event.edit(saved_messages.get_message(name))


client.start()
client.run_until_disconnected()
